import {
  DiceRollerDialogue
} from "./dialogue-diceRoller.js";
import {
  ActorMtA
} from "./actor.js";
/**
 * Override and extend the basic :class:`Item` implementation
 */
export class ItemMtA extends Item {
  
  /* -------------------------------------------- */
  /*	Data Preparation														*/
  /* -------------------------------------------- */

  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareData() {
    super.prepareData(); //TODO: Put this functionality in where the item is initialised to avoid problems. Alternative: isImgInitialised flag
    if (!this.img || this.img === "icons/svg/item-bag.svg" || this.img.startsWith('systems/mta/icons/placeholders')){
    
      let img = 'systems/mta/icons/placeholders/item-placeholder.svg';
      let type = this.type;
      if(type === "melee"){
        if(this.system.weaponType === "Unarmed") type = "unarmed";
        else if(this.system.weaponType === "Thrown") type = "thrown";
      }
      else if(type === "tilt"){
        if(this.system.isEnvironmental) type = "environmental";
      }
      else if(this.type === "rite"){
        if(this.system.riteType !== "rite") type = "miracle";
      }
      else if(this.type === "spell" || this.type === "attainment" || this.type === "activeSpell"){
        type = this.system.arcanum;
      }
      else if(this.type === "facet"){
        if(this.system.giftType === "moon") type = "moonGift";
        else if(this.system.giftType === "shadow") type = "shadowGift";
        else if(this.system.giftType === "wolf") type = "wolfGift";
      }
      else if(this.type === "werewolf_rite"){
        if(this.system.riteType === "Wolf Rite") type = "werewolf_rite";
        else  type = "pack_rite";
      }
      else if(this.type === "demonPower"){
        if(this.system.lore) {
          this.img = `systems/mta/icons/placeholders/${this.system.lore.replace(/\s+/g, '')}.svg`;
          return;
        }
      }
      
      img = CONFIG.MTA.placeholders.get(type);
      if(!img) img = 'systems/mta/icons/placeholders/item-placeholder.svg';

      this.img = img; 
    }
  }

  /* -------------------------------------------- */

  getRollTraits() {
    // FIXME: Currently this will only get the default traits if no dice bonus is defined.
    // Not sure if that is good behaviour..

    if(this.system.dicePool && (this.system.dicePool?.attributes?.length > 0 || this.system.dicePool.value)) { 
      return {traits: this.system.dicePool.attributes, diceBonus: this.system.dicePool.value};
    }
    const defaultTraits = { // TODO: Move this into Config?
      firearm: ["attributes_physical.dexterity", "skills_physical.firearms"],
      melee: {
        Unarmed: ["attributes_physical.strength", "skills_physical.brawl"],
        Thrown: ["attributes_physical.dexterity", "skills_physical.athletics"],
        default: ["attributes_physical.strength", "skills_physical.weaponry"],
      },
      explosive: ["attributes_physical.dexterity", "skills_physical.athletics"],
      influence: ["eph_physical.power", "eph_social.finesse"],
      manifestation: ["eph_physical.power", "eph_social.finesse"],
      vehicle: ["attributes_physical.dexterity", "skills_physical.drive"],
      numen: ["eph_physical.power", "eph_social.finesse"]
    };

    let traits = [];
  
    if (this.type in defaultTraits) {
      const typeData = defaultTraits[this.type];
      if (Array.isArray(typeData)) {
        traits = typeData;
      } else if (this.system.weaponType in typeData) {
        traits = typeData[this.system.weaponType];
      } else {
        traits = typeData.default;
      }
    }
  
    return {traits, diceBonus: 0};
  }

  isWeapon() {
    return this.type === "firearm" || this.type === "melee" || this.system.isWeapon || this.type === "explosive" || this.type === "combat_dice_pool";
  }

  async showChatCard() {
    const token = this.actor.token;
     const templateData = {
       item: this,
       actor: this.actor,
       tokenId: token ? `${token.object.scene.id}.${token.id}` : null, //token ? `${token.scene.id}.${token.id}` : null,
       isSpell: this.type === "spell",
       data: await this.getChatData()
     };
 
     // Render the chat card template
     const template = `systems/mta/templates/chat/item-card.html`;
     const html = await renderTemplate(template, templateData);
 
     // Basic chat message data
     const chatData = {
       user: game.user.id,
       type: CONST.CHAT_MESSAGE_TYPES.OTHER,
       content: html,
       speaker: ChatMessage.getSpeaker({actor: this.actor, token: this.actor.token}),
       flavor: ""
     };
     
     // Toggle default roll mode
     let rollMode = game.settings.get("core", "rollMode");
     if ( ["gmroll", "blindroll"].includes(rollMode) ) chatData["whisper"] = ChatMessage.getWhisperRecipients("GM");
     if ( rollMode === "blindroll" ) chatData["blind"] = true;
    ChatMessage.create(chatData);
  }

  async roll(target, quickRoll=false) {
    // TODO: Combine item description and roll

    if(this.type !== "combat_dice_pool") this.showChatCard();

    if(!this.actor) {
      ui.notifications.error(`CofD: An item can only be rolled if an actor owns it!`);
      return;
    }

    if(!target) { // Infer a target from the selected targets
      const targets = game.user.targets;
      target = targets ? targets.values().next().value : undefined;
    }

    let macro;
    if(this.system.dicePool?.macro) {
      macro = game.macros.get(this.system.dicePool.macro);
      await this.actor.setFlag('mta', 'lastRolledItem', this.id);
    }

    const {traits, diceBonus} = this.getRollTraits();

    if(!traits.length && !diceBonus && this.type !== "combat_dice_pool" && this.type !== "general_dice_pool") {
      if(macro) macro.execute({actor: this.actor, token: this.actor.token ?? this.actor.getActiveTokens[0]});
      return;
    }

    let {dicePool, flavor} = this.actor.assembleDicePool({traits, diceBonus});
    if(!flavor) flavor = "Skill Check";

    let extended = false,
    defense = 0;

    if(this.system.diceBonus) {
      dicePool += this.system.diceBonus;
      flavor += this.system.diceBonus >= 0 ? ' (+' : ' ('; 
      flavor += this.system.diceBonus + ' equipment bonus)';
    }

    if(this.type === "combat_dice_pool" || this.type === "general_dice_pool") {
      dicePool = +this.system.value;
    }
    console.log("SORRY WAT",dicePool)

    if(this.system.dicePool?.extended) {
      extended = true;
    }

    const damageRoll = this.isWeapon();
    if(damageRoll) {
      if(target?.actor?.system.derivedTraits) { // Remove target defense
        const def = target.actor.system.derivedTraits.defense;
        defense = (def.final ? def.final : def.value);
      }
      if(target) {
        flavor += " vs target " + target.name;
      }
    }

    if(this.type === "explosive") { // Create measured templates for explosives
      const distance = this.system.blastArea > 0 ? this.system.blastArea : 1;
      const pos = target ? {x: target.center.x, y: target.center.y} : canvas.scene._viewPosition; // Default is middle of the screen

      const getSurroundingTokens = (origin, distance=1) => {
        return canvas.scene.tokens.filter(t => canvas.grid.measureDistance(origin, t, {gridSpaces: true}) <= distance);
      }

      let templateData = {
        t: "circle",
        user: game.user._id,
        direction: 0,
        x: pos.x,
        y: pos.y,
        flags: {
            world: {
                measuredTemplate: true,
            },
        },
      };
      // Bashing
      canvas.scene.createEmbeddedDocuments("MeasuredTemplate", [{...templateData, distance: distance*2, fillColor: game.settings.get( "mta", "tokenHealthColorBashing")}])
      // Lethal
      canvas.scene.createEmbeddedDocuments("MeasuredTemplate", [{...templateData, distance, fillColor: game.settings.get( "mta", "tokenHealthColorLethal" )}])
    
      const surr_aggravated = getSurroundingTokens(pos, 1);
      const string_aggr = surr_aggravated.reduce((acc, cur) => {
        const roll = DiceRollerDialogue._roll(this.system.force);
        const damage = roll.total > 0 ? this.system.damage*2 : this.system.damage;
        return acc + `<li>@UUID[${cur.uuid}]{${cur.name}}: ${damage} aggravated damage (Force: ${roll.total} successes)</li>`
      }, '');

      const surr_lethal = getSurroundingTokens(pos, distance).filter(t => !surr_aggravated.includes(t));
      const string_lethal = surr_lethal.reduce((acc, cur) => {
        const roll = DiceRollerDialogue._roll(this.system.force);
        const damage = roll.total > 0 ? this.system.damage*2 : this.system.damage;
        return acc + `<li>@UUID[${cur.uuid}]{${cur.name}}: ${damage} lethal damage (Force: ${roll.total} successes)</li>`
      }, '');


      const surr_bashing = getSurroundingTokens(pos, distance*2).filter(t => !surr_lethal.includes(t) && !surr_aggravated.includes(t));
      const string_bashing = surr_bashing.reduce((acc, cur) => {
        const roll = DiceRollerDialogue._roll(this.system.force);
        const damage = roll.total > 0 ? this.system.damage*2 : this.system.damage;
        return acc + `<li>@UUID[${cur.uuid}]{${cur.name}}: ${damage} bashing damage (Force: ${roll.total} successes)</li>`
      }, '');

      ChatMessage.create({
        content: `<ul>
          ${string_aggr + string_lethal + string_bashing}
        </ul>`,
        speaker: ChatMessage.getSpeaker({actor: this.actor}),
      });
    }

    const ballistic = target ? target.actor?.system.derivedTraits.ballistic.final : 0;
    const armor = target ? target.actor?.system.derivedTraits.armor.final : 0;
    let applyDefense = this.type === "melee" || this.type === "explosive" || (this.type === "firearm" && target?.actor?.type === "ephemeral") || this.system.applyDefense;

    if(quickRoll) {
      if(damageRoll) {
        DiceRollerDialogue.rollToChat({
          dicePool, 
          flavor, 
          title: this.name + " - " + flavor, 
          actorOverride: this.actor,
          macro,
          actor: this.actor,
          comment: this.system.dicePool?.comment
        });
      }
      else {
        DiceRollerDialogue.rollWithDamage({
          dicePool, 
          flavor, 
          title: this.name + " - " + flavor, 
          itemName: this.name, 
          itemImg: this.img, 
          itemDescr: this.system.description, 
          itemRef: this, 
          weaponDamage: +this.system.damage, 
          armorPiercing: this.system.penetration, 
          spendAmmo: this.type === "firearm", 
          actorOverride: this.actor,
          macro,
          actor: this.actor,
          comment: this.system.dicePool?.comment,
          noHitSuccessesToDamage: this.type === "explosive",
          target,
          defense,
          applyDefense,
          ignoreBallistic: this.type !== "firearm",
          armor,
          ballistic
        });
      }

    }
    else {
      let diceRoller = new DiceRollerDialogue({
        dicePool, 
        extended, 
        flavor, 
        title: this.name + " - " + flavor, 
        damageRoll, 
        itemName: this.name, 
        itemImg: this.img, 
        itemDescr: this.system.description, 
        itemRef: this, 
        weaponDamage: +this.system.damage, 
        armorPiercing: this.system.penetration, 
        spendAmmo: this.type === "firearm", 
        actorOverride: this.actor,
        macro,
        actor: this.actor,
        comment: this.system.dicePool?.comment,
        noHitSuccessesToDamage: this.type === "explosive",
        target,
        defense,
        applyDefense,
        ignoreBallistic: this.type !== "firearm",
        armor,
        ballistic
      });
      diceRoller.render(true);
    }

  }



    
  /**
   * Prepare an object of chat data used to display a card for the Item in the chat log
   * @return {Object}               An object of chat data to render
   */
  async getChatData() {
    let secrets = this.isOwner;
    if (game.user.isGM) secrets = true;
    //enrichHTML(content, secrets, entities, links, rolls, rollData) → {string}

    const data = {
      description: TextEditor.enrichHTML(this.system.description, {secrets:secrets, entities:true, async: false})
    }

    return data;
  }
  
  /* -------------------------------------------- */
  /*  Chat Message Helpers                        */
  /* -------------------------------------------- */

  static chatListeners(html) {
    html.on('click', '.button', this._onChatCardAction.bind(this));
    html.on('click', '.item-name', this._onChatCardToggleContent.bind(this));
  }
  
  /* -------------------------------------------- */

  /**
   * Handle execution of a chat card action via a click event on one of the card buttons
   * @param {Event} event       The originating click event
   * @returns {Promise}         A promise which resolves once the handler workflow is complete
   * @private
   */
  static async _onChatCardAction(event) {
    event.preventDefault();
      
    // Extract card data
    const button = event.currentTarget;
    button.disabled = true;

    const card = button.closest(".chat-card");
    if(!card) return;
    const messageId = card.closest(".message").dataset.messageId;
    const message =  game.messages.get(messageId);
    const action = button.dataset.action;

    if(action === "addActiveSpell"){
      // Validate permission to proceed with the roll
      if ( !( game.user.isGM || message.isAuthor ) ) return;

      // Get the Actor from a synthetic Token
      const actor = this._getChatCardActor(card.dataset.tokenId, card.dataset.actorId);
      if ( !actor ) return;
      
      //Get spell data
      let description = $(card).find(".card-description");
      description = description[0].innerHTML;
      
      let spellName = $(card).find(".item-name");
      spellName = spellName[0].innerText;
      
      //let image = $(card).find(".item-img");
      //image = image[0].src;
      let image = card.dataset.img;

      let spellFactorsArray = $(card).find(".spell-factors > li");
      spellFactorsArray = $.makeArray(spellFactorsArray);
      spellFactorsArray = spellFactorsArray.map(ele => {
        let text = ele.innerText;
        let advanced = ele.dataset.advanced === "true";
        let splitText = text.split(":",2);
        
        return [splitText[0], splitText[1], advanced];
      });
      let spellFactors = {};
      for(let i = 0; i < spellFactorsArray.length; i++){
        spellFactors[spellFactorsArray[i][0]] = {value: spellFactorsArray[i][1].trim(), isAdvanced: spellFactorsArray[i][2]};
      }
      
      //Special handling for conditional duration, and advanced potency
      let durationSplit = spellFactors.Duration.value.split("(",2);
      spellFactors.Duration.value = durationSplit[0];
      if(durationSplit[1]) spellFactors.Duration.condition = durationSplit[1].split(")",1)[0];
      spellFactors.Potency.value = spellFactors.Potency.value.split("(",1)[0].trim();
      
      const spellData = {
        name: spellName,
        img: image,
        system: {
          potency: spellFactors.Potency,
          duration: spellFactors.Duration,
          scale: spellFactors.Scale,
          arcanum: card.dataset.arcanum, 
          level: card.dataset.level, 
          practice: card.dataset.practice, 
          primaryFactor: card.dataset.primfactor, 
          withstand: card.dataset.withstand,
          description: description
        }
      };
      
      //Add spell to active spells
      const activeSpellData = mergeObject(spellData, {type: "activeSpell"},{insertKeys: true,overwrite: true,inplace: false,enforceTypes: true});
      await actor.createEmbeddedDocuments("Item", [activeSpellData]);
      ui.notifications.info("Spell added to active spells of " + actor.name);
    }
    else if(action === "applyDamage") {

      // Get target
      const tokenId = button.dataset.tokenid;
      const actorId = button.dataset.actorid;
      const actor = this._getChatCardActor(tokenId, actorId);
      let damage = +button.dataset.damage;
      let damageBashing = +button.dataset.bashingdamageinflicted;

      if ( !actor?.isOwner ) {
        ui.notifications.warn("Can currently only be executed by the GM or the actor owner!");
        return;
      }
      console.log(actor.system, button.dataset)


      if(damageBashing) await actor.damage(damageBashing, "bashing");
      if(damage) actor.damage(damage, "lethal");
      

      console.log("target", actor, tokenId, actorId)
    }
    // Re-enable the button
    button.disabled = false;
  }
  
  /* -------------------------------------------- */

  /**
   * Get the Actor which is the author of a chat card
   * @param {HTMLElement} card    The chat card being used
   * @return {Actor|null}         The Actor entity or null
   * @private
   */
  static _getChatCardActor(tokenKey, actorId) {

    // Case 1 - a synthetic actor from a Token
    if (tokenKey) {
      const [sceneId, tokenId] = tokenKey.split(".");
      const scene = game.scenes.get(sceneId);
      if (!scene) return null;
      const tokenData = scene.getEmbeddedEntity("Token", tokenId);
      if (!tokenData) return null;
      const token = new Token(tokenData);
      return token.actor;
    }

    // Case 2 - use Actor ID directory
    return game.actors.get(actorId) || null;
  }
  
  /**
   * Handle toggling the visibility of chat card content when the name is clicked
   * @param {Event} event   The originating click event
   * @private
   */
  static _onChatCardToggleContent(event) {
    event.preventDefault();
    const header = event.currentTarget;
    const card = header.closest(".chat-card");
    const content = card.querySelector(".card-description");
    content.style.display = content.style.display === "none" ? "block" : "none";
  }
}
